package com.theseus.pds.service.core.service.dto;

import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.theseus.pds.service.core.domain.Medicine} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class MedicineDTO implements Serializable {

    private String id;

    @NotNull
    private String uuid;

    @NotNull
    private String name;

    @NotNull
    private String genericName;

    @NotNull
    private String drugType;

    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MedicineDTO)) {
            return false;
        }

        MedicineDTO medicineDTO = (MedicineDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, medicineDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MedicineDTO{" +
            "id='" + getId() + "'" +
            ", uuid='" + getUuid() + "'" +
            ", name='" + getName() + "'" +
            ", genericName='" + getGenericName() + "'" +
            ", drugType='" + getDrugType() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
