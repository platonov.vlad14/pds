package com.theseus.pds.service.core.service.mapper;

import com.theseus.pds.service.core.domain.Medicine;
import com.theseus.pds.service.core.service.dto.MedicineDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Medicine} and its DTO {@link MedicineDTO}.
 */
@Mapper(componentModel = "spring")
public interface MedicineMapper extends EntityMapper<MedicineDTO, Medicine> {}
