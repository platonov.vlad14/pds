package com.theseus.pds.service.core.service;

import com.theseus.pds.service.core.service.dto.ReceiptDTO;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.theseus.pds.service.core.domain.Receipt}.
 */
public interface ReceiptService {
    /**
     * Save a receipt.
     *
     * @param receiptDTO the entity to save.
     * @return the persisted entity.
     */
    ReceiptDTO save(ReceiptDTO receiptDTO);

    /**
     * Updates a receipt.
     *
     * @param receiptDTO the entity to update.
     * @return the persisted entity.
     */
    ReceiptDTO update(ReceiptDTO receiptDTO);

    /**
     * Partially updates a receipt.
     *
     * @param receiptDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ReceiptDTO> partialUpdate(ReceiptDTO receiptDTO);

    /**
     * Get all the receipts.
     *
     * @return the list of entities.
     */
    List<ReceiptDTO> findAll();

    /**
     * Get all the receipts with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ReceiptDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" receipt.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ReceiptDTO> findOne(String id);

    /**
     * Delete the "id" receipt.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
