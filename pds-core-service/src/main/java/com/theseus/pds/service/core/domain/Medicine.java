package com.theseus.pds.service.core.domain;

import jakarta.validation.constraints.*;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Medicine.
 */
@Document(collection = "medicine")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Medicine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("uuid")
    private String uuid;

    @NotNull
    @Field("name")
    private String name;

    @NotNull
    @Field("generic_name")
    private String genericName;

    @NotNull
    @Field("drug_type")
    private String drugType;

    @Field("description")
    private String description;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getId() {
        return this.id;
    }

    public Medicine id(String id) {
        this.setId(id);
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return this.uuid;
    }

    public Medicine uuid(String uuid) {
        this.setUuid(uuid);
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return this.name;
    }

    public Medicine name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenericName() {
        return this.genericName;
    }

    public Medicine genericName(String genericName) {
        this.setGenericName(genericName);
        return this;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getDrugType() {
        return this.drugType;
    }

    public Medicine drugType(String drugType) {
        this.setDrugType(drugType);
        return this;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getDescription() {
        return this.description;
    }

    public Medicine description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Medicine)) {
            return false;
        }
        return getId() != null && getId().equals(((Medicine) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Medicine{" +
            "id=" + getId() +
            ", uuid='" + getUuid() + "'" +
            ", name='" + getName() + "'" +
            ", genericName='" + getGenericName() + "'" +
            ", drugType='" + getDrugType() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
