package com.theseus.pds.service.core.service;

import com.theseus.pds.service.core.service.dto.MedicineDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.theseus.pds.service.core.domain.Medicine}.
 */
public interface MedicineService {
    /**
     * Save a medicine.
     *
     * @param medicineDTO the entity to save.
     * @return the persisted entity.
     */
    MedicineDTO save(MedicineDTO medicineDTO);

    /**
     * Updates a medicine.
     *
     * @param medicineDTO the entity to update.
     * @return the persisted entity.
     */
    MedicineDTO update(MedicineDTO medicineDTO);

    /**
     * Partially updates a medicine.
     *
     * @param medicineDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<MedicineDTO> partialUpdate(MedicineDTO medicineDTO);

    /**
     * Get all the medicines.
     *
     * @return the list of entities.
     */
    List<MedicineDTO> findAll();

    /**
     * Get the "id" medicine.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MedicineDTO> findOne(String id);

    /**
     * Delete the "id" medicine.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
