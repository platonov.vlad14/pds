package com.theseus.pds.service.core.service.impl;

import com.theseus.pds.service.core.domain.Prescription;
import com.theseus.pds.service.core.repository.PrescriptionRepository;
import com.theseus.pds.service.core.service.PrescriptionService;
import com.theseus.pds.service.core.service.dto.PrescriptionDTO;
import com.theseus.pds.service.core.service.mapper.PrescriptionMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link com.theseus.pds.service.core.domain.Prescription}.
 */
@Service
public class PrescriptionServiceImpl implements PrescriptionService {

    private final Logger log = LoggerFactory.getLogger(PrescriptionServiceImpl.class);

    private final PrescriptionRepository prescriptionRepository;

    private final PrescriptionMapper prescriptionMapper;

    public PrescriptionServiceImpl(PrescriptionRepository prescriptionRepository, PrescriptionMapper prescriptionMapper) {
        this.prescriptionRepository = prescriptionRepository;
        this.prescriptionMapper = prescriptionMapper;
    }

    @Override
    public PrescriptionDTO save(PrescriptionDTO prescriptionDTO) {
        log.debug("Request to save Prescription : {}", prescriptionDTO);
        Prescription prescription = prescriptionMapper.toEntity(prescriptionDTO);
        prescription = prescriptionRepository.save(prescription);
        return prescriptionMapper.toDto(prescription);
    }

    @Override
    public PrescriptionDTO update(PrescriptionDTO prescriptionDTO) {
        log.debug("Request to update Prescription : {}", prescriptionDTO);
        Prescription prescription = prescriptionMapper.toEntity(prescriptionDTO);
        prescription = prescriptionRepository.save(prescription);
        return prescriptionMapper.toDto(prescription);
    }

    @Override
    public Optional<PrescriptionDTO> partialUpdate(PrescriptionDTO prescriptionDTO) {
        log.debug("Request to partially update Prescription : {}", prescriptionDTO);

        return prescriptionRepository
            .findById(prescriptionDTO.getId())
            .map(existingPrescription -> {
                prescriptionMapper.partialUpdate(existingPrescription, prescriptionDTO);

                return existingPrescription;
            })
            .map(prescriptionRepository::save)
            .map(prescriptionMapper::toDto);
    }

    @Override
    public List<PrescriptionDTO> findAll() {
        log.debug("Request to get all Prescriptions");
        return prescriptionRepository.findAll().stream().map(prescriptionMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    public Page<PrescriptionDTO> findAllWithEagerRelationships(Pageable pageable) {
        return prescriptionRepository.findAllWithEagerRelationships(pageable).map(prescriptionMapper::toDto);
    }

    @Override
    public Optional<PrescriptionDTO> findOne(String id) {
        log.debug("Request to get Prescription : {}", id);
        return prescriptionRepository.findOneWithEagerRelationships(id).map(prescriptionMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Prescription : {}", id);
        prescriptionRepository.deleteById(id);
    }
}
