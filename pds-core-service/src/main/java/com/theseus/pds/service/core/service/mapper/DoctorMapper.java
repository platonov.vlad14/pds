package com.theseus.pds.service.core.service.mapper;

import com.theseus.pds.service.core.domain.Doctor;
import com.theseus.pds.service.core.service.dto.DoctorDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Doctor} and its DTO {@link DoctorDTO}.
 */
@Mapper(componentModel = "spring")
public interface DoctorMapper extends EntityMapper<DoctorDTO, Doctor> {}
