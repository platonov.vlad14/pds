package com.theseus.pds.service.core.repository;

import com.theseus.pds.service.core.domain.Medicine;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Medicine entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedicineRepository extends MongoRepository<Medicine, String> {}
