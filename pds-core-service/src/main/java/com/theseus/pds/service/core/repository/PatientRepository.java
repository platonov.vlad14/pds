package com.theseus.pds.service.core.repository;

import com.theseus.pds.service.core.domain.Patient;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Patient entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatientRepository extends MongoRepository<Patient, String> {}
