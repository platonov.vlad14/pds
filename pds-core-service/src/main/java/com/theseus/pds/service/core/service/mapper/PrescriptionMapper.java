package com.theseus.pds.service.core.service.mapper;

import com.theseus.pds.service.core.domain.Doctor;
import com.theseus.pds.service.core.domain.Patient;
import com.theseus.pds.service.core.domain.Prescription;
import com.theseus.pds.service.core.service.dto.DoctorDTO;
import com.theseus.pds.service.core.service.dto.PatientDTO;
import com.theseus.pds.service.core.service.dto.PrescriptionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Prescription} and its DTO {@link PrescriptionDTO}.
 */
@Mapper(componentModel = "spring")
public interface PrescriptionMapper extends EntityMapper<PrescriptionDTO, Prescription> {
    @Mapping(target = "patient", source = "patient", qualifiedByName = "patientFirstName")
    @Mapping(target = "doctor", source = "doctor", qualifiedByName = "doctorFirstName")
    PrescriptionDTO toDto(Prescription s);

    @Named("patientFirstName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "firstName", source = "firstName")
    PatientDTO toDtoPatientFirstName(Patient patient);

    @Named("doctorFirstName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "firstName", source = "firstName")
    DoctorDTO toDtoDoctorFirstName(Doctor doctor);
}
