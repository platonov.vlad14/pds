package com.theseus.pds.service.core.repository;

import com.theseus.pds.service.core.domain.Doctor;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Doctor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DoctorRepository extends MongoRepository<Doctor, String> {}
