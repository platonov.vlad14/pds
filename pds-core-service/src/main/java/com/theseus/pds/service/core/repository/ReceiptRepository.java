package com.theseus.pds.service.core.repository;

import com.theseus.pds.service.core.domain.Receipt;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Receipt entity.
 */
@Repository
public interface ReceiptRepository extends MongoRepository<Receipt, String> {
    @Query("{}")
    Page<Receipt> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Receipt> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Receipt> findOneWithEagerRelationships(String id);
}
