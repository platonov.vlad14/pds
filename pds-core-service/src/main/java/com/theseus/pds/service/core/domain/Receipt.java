package com.theseus.pds.service.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Receipt.
 */
@Document(collection = "receipt")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Receipt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("is_redeemed")
    private Boolean isRedeemed;

    @NotNull
    @Field("dosage")
    private Integer dosage;

    @NotNull
    @Field("timing")
    private String timing;

    @NotNull
    @Field("duration")
    private String duration;

    @DBRef
    @Field("medicine")
    private Medicine medicine;

    @DBRef
    @Field("prescription")
    @JsonIgnoreProperties(value = { "receipts", "patient", "doctor" }, allowSetters = true)
    private Prescription prescription;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getId() {
        return this.id;
    }

    public Receipt id(String id) {
        this.setId(id);
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsRedeemed() {
        return this.isRedeemed;
    }

    public Receipt isRedeemed(Boolean isRedeemed) {
        this.setIsRedeemed(isRedeemed);
        return this;
    }

    public void setIsRedeemed(Boolean isRedeemed) {
        this.isRedeemed = isRedeemed;
    }

    public Integer getDosage() {
        return this.dosage;
    }

    public Receipt dosage(Integer dosage) {
        this.setDosage(dosage);
        return this;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public String getTiming() {
        return this.timing;
    }

    public Receipt timing(String timing) {
        this.setTiming(timing);
        return this;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public String getDuration() {
        return this.duration;
    }

    public Receipt duration(String duration) {
        this.setDuration(duration);
        return this;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Medicine getMedicine() {
        return this.medicine;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    public Receipt medicine(Medicine medicine) {
        this.setMedicine(medicine);
        return this;
    }

    public Prescription getPrescription() {
        return this.prescription;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }

    public Receipt prescription(Prescription prescription) {
        this.setPrescription(prescription);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Receipt)) {
            return false;
        }
        return getId() != null && getId().equals(((Receipt) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Receipt{" +
            "id=" + getId() +
            ", isRedeemed='" + getIsRedeemed() + "'" +
            ", dosage=" + getDosage() +
            ", timing='" + getTiming() + "'" +
            ", duration='" + getDuration() + "'" +
            "}";
    }
}
