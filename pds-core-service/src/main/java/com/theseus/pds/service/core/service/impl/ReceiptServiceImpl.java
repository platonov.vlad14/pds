package com.theseus.pds.service.core.service.impl;

import com.theseus.pds.service.core.domain.Receipt;
import com.theseus.pds.service.core.repository.ReceiptRepository;
import com.theseus.pds.service.core.service.ReceiptService;
import com.theseus.pds.service.core.service.dto.ReceiptDTO;
import com.theseus.pds.service.core.service.mapper.ReceiptMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link com.theseus.pds.service.core.domain.Receipt}.
 */
@Service
public class ReceiptServiceImpl implements ReceiptService {

    private final Logger log = LoggerFactory.getLogger(ReceiptServiceImpl.class);

    private final ReceiptRepository receiptRepository;

    private final ReceiptMapper receiptMapper;

    public ReceiptServiceImpl(ReceiptRepository receiptRepository, ReceiptMapper receiptMapper) {
        this.receiptRepository = receiptRepository;
        this.receiptMapper = receiptMapper;
    }

    @Override
    public ReceiptDTO save(ReceiptDTO receiptDTO) {
        log.debug("Request to save Receipt : {}", receiptDTO);
        Receipt receipt = receiptMapper.toEntity(receiptDTO);
        receipt = receiptRepository.save(receipt);
        return receiptMapper.toDto(receipt);
    }

    @Override
    public ReceiptDTO update(ReceiptDTO receiptDTO) {
        log.debug("Request to update Receipt : {}", receiptDTO);
        Receipt receipt = receiptMapper.toEntity(receiptDTO);
        receipt = receiptRepository.save(receipt);
        return receiptMapper.toDto(receipt);
    }

    @Override
    public Optional<ReceiptDTO> partialUpdate(ReceiptDTO receiptDTO) {
        log.debug("Request to partially update Receipt : {}", receiptDTO);

        return receiptRepository
            .findById(receiptDTO.getId())
            .map(existingReceipt -> {
                receiptMapper.partialUpdate(existingReceipt, receiptDTO);

                return existingReceipt;
            })
            .map(receiptRepository::save)
            .map(receiptMapper::toDto);
    }

    @Override
    public List<ReceiptDTO> findAll() {
        log.debug("Request to get all Receipts");
        return receiptRepository.findAll().stream().map(receiptMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    public Page<ReceiptDTO> findAllWithEagerRelationships(Pageable pageable) {
        return receiptRepository.findAllWithEagerRelationships(pageable).map(receiptMapper::toDto);
    }

    @Override
    public Optional<ReceiptDTO> findOne(String id) {
        log.debug("Request to get Receipt : {}", id);
        return receiptRepository.findOneWithEagerRelationships(id).map(receiptMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Receipt : {}", id);
        receiptRepository.deleteById(id);
    }
}
