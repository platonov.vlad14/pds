package com.theseus.pds.service.core.repository;

import com.theseus.pds.service.core.domain.Prescription;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Prescription entity.
 */
@Repository
public interface PrescriptionRepository extends MongoRepository<Prescription, String> {
    @Query("{}")
    Page<Prescription> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Prescription> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Prescription> findOneWithEagerRelationships(String id);
}
