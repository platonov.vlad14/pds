package com.theseus.pds.service.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Prescription.
 */
@Document(collection = "prescription")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Prescription implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("code")
    private Long code;

    @NotNull
    @Field("status")
    private String status;

    @NotNull
    @Field("name")
    private String name;

    @Field("description")
    private String description;

    @Field("issue_date")
    private LocalDate issueDate;

    @Field("expiry_date")
    private LocalDate expiryDate;

    @DBRef
    @Field("receipts")
    @JsonIgnoreProperties(value = { "medicine", "prescription" }, allowSetters = true)
    private Set<Receipt> receipts = new HashSet<>();

    @DBRef
    @Field("patient")
    private Patient patient;

    @DBRef
    @Field("doctor")
    private Doctor doctor;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getId() {
        return this.id;
    }

    public Prescription id(String id) {
        this.setId(id);
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCode() {
        return this.code;
    }

    public Prescription code(Long code) {
        this.setCode(code);
        return this;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getStatus() {
        return this.status;
    }

    public Prescription status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return this.name;
    }

    public Prescription name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public Prescription description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getIssueDate() {
        return this.issueDate;
    }

    public Prescription issueDate(LocalDate issueDate) {
        this.setIssueDate(issueDate);
        return this;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDate getExpiryDate() {
        return this.expiryDate;
    }

    public Prescription expiryDate(LocalDate expiryDate) {
        this.setExpiryDate(expiryDate);
        return this;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Set<Receipt> getReceipts() {
        return this.receipts;
    }

    public void setReceipts(Set<Receipt> receipts) {
        if (this.receipts != null) {
            this.receipts.forEach(i -> i.setPrescription(null));
        }
        if (receipts != null) {
            receipts.forEach(i -> i.setPrescription(this));
        }
        this.receipts = receipts;
    }

    public Prescription receipts(Set<Receipt> receipts) {
        this.setReceipts(receipts);
        return this;
    }

    public Prescription addReceipts(Receipt receipt) {
        this.receipts.add(receipt);
        receipt.setPrescription(this);
        return this;
    }

    public Prescription removeReceipts(Receipt receipt) {
        this.receipts.remove(receipt);
        receipt.setPrescription(null);
        return this;
    }

    public Patient getPatient() {
        return this.patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Prescription patient(Patient patient) {
        this.setPatient(patient);
        return this;
    }

    public Doctor getDoctor() {
        return this.doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Prescription doctor(Doctor doctor) {
        this.setDoctor(doctor);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Prescription)) {
            return false;
        }
        return getId() != null && getId().equals(((Prescription) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Prescription{" +
            "id=" + getId() +
            ", code=" + getCode() +
            ", status='" + getStatus() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", issueDate='" + getIssueDate() + "'" +
            ", expiryDate='" + getExpiryDate() + "'" +
            "}";
    }
}
