package com.theseus.pds.service.core.service.mapper;

import com.theseus.pds.service.core.domain.Medicine;
import com.theseus.pds.service.core.domain.Prescription;
import com.theseus.pds.service.core.domain.Receipt;
import com.theseus.pds.service.core.service.dto.MedicineDTO;
import com.theseus.pds.service.core.service.dto.PrescriptionDTO;
import com.theseus.pds.service.core.service.dto.ReceiptDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Receipt} and its DTO {@link ReceiptDTO}.
 */
@Mapper(componentModel = "spring")
public interface ReceiptMapper extends EntityMapper<ReceiptDTO, Receipt> {
    @Mapping(target = "medicine", source = "medicine", qualifiedByName = "medicineName")
    @Mapping(target = "prescription", source = "prescription", qualifiedByName = "prescriptionName")
    ReceiptDTO toDto(Receipt s);

    @Named("medicineName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    MedicineDTO toDtoMedicineName(Medicine medicine);

    @Named("prescriptionName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    PrescriptionDTO toDtoPrescriptionName(Prescription prescription);
}
