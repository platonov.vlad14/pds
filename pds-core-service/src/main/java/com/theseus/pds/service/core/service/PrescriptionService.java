package com.theseus.pds.service.core.service;

import com.theseus.pds.service.core.service.dto.PrescriptionDTO;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.theseus.pds.service.core.domain.Prescription}.
 */
public interface PrescriptionService {
    /**
     * Save a prescription.
     *
     * @param prescriptionDTO the entity to save.
     * @return the persisted entity.
     */
    PrescriptionDTO save(PrescriptionDTO prescriptionDTO);

    /**
     * Updates a prescription.
     *
     * @param prescriptionDTO the entity to update.
     * @return the persisted entity.
     */
    PrescriptionDTO update(PrescriptionDTO prescriptionDTO);

    /**
     * Partially updates a prescription.
     *
     * @param prescriptionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PrescriptionDTO> partialUpdate(PrescriptionDTO prescriptionDTO);

    /**
     * Get all the prescriptions.
     *
     * @return the list of entities.
     */
    List<PrescriptionDTO> findAll();

    /**
     * Get all the prescriptions with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PrescriptionDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" prescription.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PrescriptionDTO> findOne(String id);

    /**
     * Delete the "id" prescription.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
