package com.theseus.pds.service.core.service.impl;

import com.theseus.pds.service.core.domain.Medicine;
import com.theseus.pds.service.core.repository.MedicineRepository;
import com.theseus.pds.service.core.service.MedicineService;
import com.theseus.pds.service.core.service.dto.MedicineDTO;
import com.theseus.pds.service.core.service.mapper.MedicineMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link com.theseus.pds.service.core.domain.Medicine}.
 */
@Service
public class MedicineServiceImpl implements MedicineService {

    private final Logger log = LoggerFactory.getLogger(MedicineServiceImpl.class);

    private final MedicineRepository medicineRepository;

    private final MedicineMapper medicineMapper;

    public MedicineServiceImpl(MedicineRepository medicineRepository, MedicineMapper medicineMapper) {
        this.medicineRepository = medicineRepository;
        this.medicineMapper = medicineMapper;
    }

    @Override
    public MedicineDTO save(MedicineDTO medicineDTO) {
        log.debug("Request to save Medicine : {}", medicineDTO);
        Medicine medicine = medicineMapper.toEntity(medicineDTO);
        medicine = medicineRepository.save(medicine);
        return medicineMapper.toDto(medicine);
    }

    @Override
    public MedicineDTO update(MedicineDTO medicineDTO) {
        log.debug("Request to update Medicine : {}", medicineDTO);
        Medicine medicine = medicineMapper.toEntity(medicineDTO);
        medicine = medicineRepository.save(medicine);
        return medicineMapper.toDto(medicine);
    }

    @Override
    public Optional<MedicineDTO> partialUpdate(MedicineDTO medicineDTO) {
        log.debug("Request to partially update Medicine : {}", medicineDTO);

        return medicineRepository
            .findById(medicineDTO.getId())
            .map(existingMedicine -> {
                medicineMapper.partialUpdate(existingMedicine, medicineDTO);

                return existingMedicine;
            })
            .map(medicineRepository::save)
            .map(medicineMapper::toDto);
    }

    @Override
    public List<MedicineDTO> findAll() {
        log.debug("Request to get all Medicines");
        return medicineRepository.findAll().stream().map(medicineMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<MedicineDTO> findOne(String id) {
        log.debug("Request to get Medicine : {}", id);
        return medicineRepository.findById(id).map(medicineMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Medicine : {}", id);
        medicineRepository.deleteById(id);
    }
}
