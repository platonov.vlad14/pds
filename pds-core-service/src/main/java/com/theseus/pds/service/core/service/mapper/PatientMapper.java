package com.theseus.pds.service.core.service.mapper;

import com.theseus.pds.service.core.domain.Patient;
import com.theseus.pds.service.core.service.dto.PatientDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Patient} and its DTO {@link PatientDTO}.
 */
@Mapper(componentModel = "spring")
public interface PatientMapper extends EntityMapper<PatientDTO, Patient> {}
