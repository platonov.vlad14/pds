package com.theseus.pds.service.core.domain;

import java.util.UUID;

public class PatientTestSamples {

    public static Patient getPatientSample1() {
        return new Patient()
            .id("id1")
            .firstName("firstName1")
            .lastName("lastName1")
            .gender("gender1")
            .contactNumber("contactNumber1")
            .address("address1");
    }

    public static Patient getPatientSample2() {
        return new Patient()
            .id("id2")
            .firstName("firstName2")
            .lastName("lastName2")
            .gender("gender2")
            .contactNumber("contactNumber2")
            .address("address2");
    }

    public static Patient getPatientRandomSampleGenerator() {
        return new Patient()
            .id(UUID.randomUUID().toString())
            .firstName(UUID.randomUUID().toString())
            .lastName(UUID.randomUUID().toString())
            .gender(UUID.randomUUID().toString())
            .contactNumber(UUID.randomUUID().toString())
            .address(UUID.randomUUID().toString());
    }
}
