package com.theseus.pds.service.core.domain;

import static com.theseus.pds.service.core.domain.DoctorTestSamples.*;
import static com.theseus.pds.service.core.domain.PatientTestSamples.*;
import static com.theseus.pds.service.core.domain.PrescriptionTestSamples.*;
import static com.theseus.pds.service.core.domain.ReceiptTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.theseus.pds.service.core.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class PrescriptionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Prescription.class);
        Prescription prescription1 = getPrescriptionSample1();
        Prescription prescription2 = new Prescription();
        assertThat(prescription1).isNotEqualTo(prescription2);

        prescription2.setId(prescription1.getId());
        assertThat(prescription1).isEqualTo(prescription2);

        prescription2 = getPrescriptionSample2();
        assertThat(prescription1).isNotEqualTo(prescription2);
    }

    @Test
    void receiptsTest() throws Exception {
        Prescription prescription = getPrescriptionRandomSampleGenerator();
        Receipt receiptBack = getReceiptRandomSampleGenerator();

        prescription.addReceipts(receiptBack);
        assertThat(prescription.getReceipts()).containsOnly(receiptBack);
        assertThat(receiptBack.getPrescription()).isEqualTo(prescription);

        prescription.removeReceipts(receiptBack);
        assertThat(prescription.getReceipts()).doesNotContain(receiptBack);
        assertThat(receiptBack.getPrescription()).isNull();

        prescription.receipts(new HashSet<>(Set.of(receiptBack)));
        assertThat(prescription.getReceipts()).containsOnly(receiptBack);
        assertThat(receiptBack.getPrescription()).isEqualTo(prescription);

        prescription.setReceipts(new HashSet<>());
        assertThat(prescription.getReceipts()).doesNotContain(receiptBack);
        assertThat(receiptBack.getPrescription()).isNull();
    }

    @Test
    void patientTest() throws Exception {
        Prescription prescription = getPrescriptionRandomSampleGenerator();
        Patient patientBack = getPatientRandomSampleGenerator();

        prescription.setPatient(patientBack);
        assertThat(prescription.getPatient()).isEqualTo(patientBack);

        prescription.patient(null);
        assertThat(prescription.getPatient()).isNull();
    }

    @Test
    void doctorTest() throws Exception {
        Prescription prescription = getPrescriptionRandomSampleGenerator();
        Doctor doctorBack = getDoctorRandomSampleGenerator();

        prescription.setDoctor(doctorBack);
        assertThat(prescription.getDoctor()).isEqualTo(doctorBack);

        prescription.doctor(null);
        assertThat(prescription.getDoctor()).isNull();
    }
}
