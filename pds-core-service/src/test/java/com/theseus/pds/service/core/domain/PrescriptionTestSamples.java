package com.theseus.pds.service.core.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class PrescriptionTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Prescription getPrescriptionSample1() {
        return new Prescription().id("id1").code(1L).status("status1").name("name1").description("description1");
    }

    public static Prescription getPrescriptionSample2() {
        return new Prescription().id("id2").code(2L).status("status2").name("name2").description("description2");
    }

    public static Prescription getPrescriptionRandomSampleGenerator() {
        return new Prescription()
            .id(UUID.randomUUID().toString())
            .code(longCount.incrementAndGet())
            .status(UUID.randomUUID().toString())
            .name(UUID.randomUUID().toString())
            .description(UUID.randomUUID().toString());
    }
}
