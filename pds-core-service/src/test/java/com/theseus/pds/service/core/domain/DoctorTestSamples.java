package com.theseus.pds.service.core.domain;

import java.util.UUID;

public class DoctorTestSamples {

    public static Doctor getDoctorSample1() {
        return new Doctor().id("id1").firstName("firstName1").lastName("lastName1").contactNumber("contactNumber1").specialty("specialty1");
    }

    public static Doctor getDoctorSample2() {
        return new Doctor().id("id2").firstName("firstName2").lastName("lastName2").contactNumber("contactNumber2").specialty("specialty2");
    }

    public static Doctor getDoctorRandomSampleGenerator() {
        return new Doctor()
            .id(UUID.randomUUID().toString())
            .firstName(UUID.randomUUID().toString())
            .lastName(UUID.randomUUID().toString())
            .contactNumber(UUID.randomUUID().toString())
            .specialty(UUID.randomUUID().toString());
    }
}
