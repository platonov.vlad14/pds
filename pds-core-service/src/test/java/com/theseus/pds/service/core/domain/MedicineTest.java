package com.theseus.pds.service.core.domain;

import static com.theseus.pds.service.core.domain.MedicineTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.theseus.pds.service.core.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MedicineTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Medicine.class);
        Medicine medicine1 = getMedicineSample1();
        Medicine medicine2 = new Medicine();
        assertThat(medicine1).isNotEqualTo(medicine2);

        medicine2.setId(medicine1.getId());
        assertThat(medicine1).isEqualTo(medicine2);

        medicine2 = getMedicineSample2();
        assertThat(medicine1).isNotEqualTo(medicine2);
    }
}
