package com.theseus.pds.service.core.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class ReceiptTestSamples {

    private static final Random random = new Random();
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Receipt getReceiptSample1() {
        return new Receipt().id("id1").dosage(1).timing("timing1").duration("duration1");
    }

    public static Receipt getReceiptSample2() {
        return new Receipt().id("id2").dosage(2).timing("timing2").duration("duration2");
    }

    public static Receipt getReceiptRandomSampleGenerator() {
        return new Receipt()
            .id(UUID.randomUUID().toString())
            .dosage(intCount.incrementAndGet())
            .timing(UUID.randomUUID().toString())
            .duration(UUID.randomUUID().toString());
    }
}
