package com.theseus.pds.service.core.domain;

import static com.theseus.pds.service.core.domain.MedicineTestSamples.*;
import static com.theseus.pds.service.core.domain.PrescriptionTestSamples.*;
import static com.theseus.pds.service.core.domain.ReceiptTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.theseus.pds.service.core.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ReceiptTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Receipt.class);
        Receipt receipt1 = getReceiptSample1();
        Receipt receipt2 = new Receipt();
        assertThat(receipt1).isNotEqualTo(receipt2);

        receipt2.setId(receipt1.getId());
        assertThat(receipt1).isEqualTo(receipt2);

        receipt2 = getReceiptSample2();
        assertThat(receipt1).isNotEqualTo(receipt2);
    }

    @Test
    void medicineTest() throws Exception {
        Receipt receipt = getReceiptRandomSampleGenerator();
        Medicine medicineBack = getMedicineRandomSampleGenerator();

        receipt.setMedicine(medicineBack);
        assertThat(receipt.getMedicine()).isEqualTo(medicineBack);

        receipt.medicine(null);
        assertThat(receipt.getMedicine()).isNull();
    }

    @Test
    void prescriptionTest() throws Exception {
        Receipt receipt = getReceiptRandomSampleGenerator();
        Prescription prescriptionBack = getPrescriptionRandomSampleGenerator();

        receipt.setPrescription(prescriptionBack);
        assertThat(receipt.getPrescription()).isEqualTo(prescriptionBack);

        receipt.prescription(null);
        assertThat(receipt.getPrescription()).isNull();
    }
}
