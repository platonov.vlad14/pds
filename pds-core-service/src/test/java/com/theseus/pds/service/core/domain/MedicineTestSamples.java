package com.theseus.pds.service.core.domain;

import java.util.UUID;

public class MedicineTestSamples {

    public static Medicine getMedicineSample1() {
        return new Medicine()
            .id("id1")
            .uuid("uuid1")
            .name("name1")
            .genericName("genericName1")
            .drugType("drugType1")
            .description("description1");
    }

    public static Medicine getMedicineSample2() {
        return new Medicine()
            .id("id2")
            .uuid("uuid2")
            .name("name2")
            .genericName("genericName2")
            .drugType("drugType2")
            .description("description2");
    }

    public static Medicine getMedicineRandomSampleGenerator() {
        return new Medicine()
            .id(UUID.randomUUID().toString())
            .uuid(UUID.randomUUID().toString())
            .name(UUID.randomUUID().toString())
            .genericName(UUID.randomUUID().toString())
            .drugType(UUID.randomUUID().toString())
            .description(UUID.randomUUID().toString());
    }
}
