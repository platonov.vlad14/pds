# PDS Docker-Compose configuration

## Usage

Move to the root of the docker-compose directory: `cd docker-compose`.


Launch all your infrastructure by running: `docker compose up -d`.

## Configured Docker services

### Service registry and configuration server:

- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:

- PdsGatewayService (gateway application)
- PdsGatewayService's postgresql database
- PdsPrescriptionCoreService (microservice application)
- PdsPrescriptionCoreService's mongodb database


